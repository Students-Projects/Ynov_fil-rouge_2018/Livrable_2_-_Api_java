import static spark.Spark.*;

public class App {
    public static void main(String[] args) {
        get("/ls/ou", (request, response) -> listou());
        get("/ls/grp", (request, response) -> listgrp());
        get("/ls/user", (request, response) -> listuser());
        post("/add/ou", (request, response) -> addou(request.contentType()));
        post("/add/grp", (request, response) -> addgrp());
        post("/add/user", (request, response) -> adduser());
        post("/modify/ou", (request, response) -> modifyou());
        post("/modify/grp", (request, response) -> modifygrp());
        post("/modify/user", (request, response) -> modifyuser());
        delete("/del/ou", (request, response) -> delou());
        delete("/del/grp", (request, response) -> delgrp());
        delete("/del/user", (request, response) -> deluser());
        post("/grp/add/user", (request, response) -> addusertogrp());
        delete("/grp/del/user", (request, response) -> delusertogrp());   
    }
    
    public static String listou() {
    	return "<h1>liste des unités organisationelles</h1>";
    }
    
    public static String listgrp() {
    	return "<h1>liste des groupes</h1>";
    }
    
    public static String listuser() {
    	return "<h1>liste des utilisateurs</h1>";
    }
    
    public static String addou(String var) {
    	if (var == "bleh") {
    			return "bruh !";
    	}
    	else {
    		return "bleh";
    	}
    }
    
    public static String addgrp() {
    	return "bleh";
    }
    
    public static String adduser() {
    	return "bleh";
    }
    
    public static String modifyou() {
    	return "bleh";
    }
    
    public static String modifygrp() {
    	return "bleh";
    }
    
    public static String modifyuser() {
    	return "bleh";
    }
    
    public static String delou() {
    	return "bleh";
    }
    
    public static String delgrp() {
    	return "bleh";
    }
    
    public static String deluser() {
    	return "bleh";
    }
    
    public static String addusertogrp() {
    	return "bleh";
    }
    
    public static String delusertogrp() {
    	return "bleh";
    }
}