# Livrable 2 : API Java

Réaliser une API en JAVA pour récupérer des informations pour appliquer des modifications sur Active Directory



Travail à faire et livrables

    -Réaliser une API en JAVA pour récupérer des informations pour appliquer des modifications sur Active Directory

Réaliser une API, en utilisant éventuellement un framework au choix, afin de proposer des fonctionnalités pour la future application mobile. Cette API devra, a minima, prendre en charge les fonctionnalités suivantes : 

    1.Lister l'ensemble des unités organisationnelles, des groupes et des utilisateurs
    2.Créer, modifier ou supprimer une unité organisationnelle
    3.Créer ou modifier un groupe en choisissant son unité organisationnelle
    4.Supprimer un groupe
    5.Créer ou modifier un utilisateur en choisissant son groupe et son unité organisationnelle
    6.Supprimer un utilisateur
    7.Ajouter un utilisateur existant à un groupe
    8.Enlever un utilisateur existant d'un groupe
    9.Ajouter/enlever un utilisateur à un groupe

Bonus : intégrer des fonctionnalités complémentaires.

Sécuriser les accès à cette API à l'aide d'une méthode d'authentification.

Procèder à l'écriture d'une documentation complète en anglais de l'API permettant son exploitation, de type http://apidocjs.com ou similaire.